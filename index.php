<?php 
	global $header_vars; 
	$header_vars = ['header_classes' => ' header--container-white'];
	$posts = new WP_Query( array(
		'post_type' => 'post',
		'orderby'=> 'date',
		'order'=> 'DESC',
		'suppress_filters' => 0
	) );
	$page_for_posts = get_option( 'page_for_posts' );

	$top_content = get_post_meta( $page_for_posts, '_top_content', true );	
?>
<?php get_header() ?>
<main class="pad-distance-between-header-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 page--title-section" data-sal="slide-down" data-sal-delay="400">
            	<?php echo apply_filters('the_content', $top_content) ?>
            </div>
            <?php if ( $posts->have_posts() ) : ?>
            	<?php while ( $posts->have_posts() ) : $posts->the_post() ?>
		            <?php get_template_part( 'templates/blogpost', 'item' ); ?>
	            <?php endwhile ?>
	        <?php endif ?>

            <div class="col-12">
                <!-- <a href="#" class="load-more-button" data-sal="slide-right" data-sal-delay="700"><?php _e('Load more', 'inone') ?></a> -->
            </div>
        </div>
    </div>
</main>
<?php get_footer() ?>