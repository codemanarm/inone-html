<?php /* Template Name: Team Template */ ?>

<?php
    global $header_vars; 
    $header_vars = ['header_classes' => ' header--container-white'];
    get_header();

    $team_members = new WP_Query( array(
        'post_type' => 'team',
        'orderby'=> 'order',
        'order'=> 'ASC',
        'suppress_filters' => 0,
        'posts_per_page' => -1
    ) );
    $team_activities = new WP_Query( array(
        'post_type' => 'team_activity',
        // 'orderby'=> 'order',
        // 'order'=> 'ASC',
        'suppress_filters' => 0,
        'posts_per_page' => -1
    ) );
    $team_activity = get_post_meta( get_the_ID(), 'page_team_activity_text', true );

?>
    <main class="pad-distance-between-header-footer">
        <div class="container">
            <div class="row">
                <div class="col-12 page--title-section" data-sal="slide-down" data-sal-delay="400">
                    <?php the_content() ?>
                </div>
                <?php if ( $team_members->have_posts() ) : ?>
                    <div class="team--members-container d-flex flex-wrap w-100 mb-5">
                        <?php while ( $team_members->have_posts() ) : $team_members->the_post() ?>
                            <?php $member_position = get_post_meta( get_the_ID(), 'team_member_position', true ); ?>
                            <div class="col-md-4 col-lg-3 col-6 flex-shrink-0">
                                <div class="team--member-wrapper" data-sal="slide-left" data-sal-delay="700">
                                    <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('medium', ['class'=> 'team--member-photo']);
                                    } ?>
                                    <div
                                        class="team--member-info d-flex flex-column align-items-center justify-content-between w-100">
                                        <h3 class="team--member-name mb-2"><?php the_title() ?></h3>
                                        <?php if(isset($member_position)): ?>
                                        <p class="team--member-position"><?php echo $member_position ?></p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
        <div class="container">
            <div class="row pl-0 pr-0">
                <div class="col-12">
                    
                    <div class="team--activity-container">
                        <div class="col-12 page--title-section" data-sal="slide-down" data-sal-delay="700">
                            <?php echo apply_filters('the_content', $team_activity) ?>
                        </div>
                        <div class="d-flex page--activity-albums">
                            <?php if ( $team_activities->have_posts() ) : ?>
                                <?php while ( $team_activities->have_posts() ) : 
                                            $team_activities->the_post(); 
                                            $images = get_post_meta( get_the_ID(), 'team_activity_images', true );
                                            ?>
                                <div class="col-md-4 col-12" data-sal="slide-left" data-sal-delay="1000">
                                    <div class="page--activity-album-item">
                                            <?php if ( has_post_thumbnail(get_the_ID()) ) :                                                 
                                                $main_img_url = get_the_post_thumbnail_url($team_activities->the_post_ID, 'full'); ?>
                                                <a href="<?php echo $main_img_url ?>" data-fancybox="album-<?php echo get_the_ID() ?>"
                                                    data-type="image" class="page--activity-album-active">
                                                        <?php the_post_thumbnail(get_the_ID(), 'medium', ['class'=> 'team--activity-cover']); ?>
                                                    <span class="page--activity-album-name">
                                                        <?php echo get_the_title(get_the_ID()); ?>
                                                    </span>
                                                </a>
                                                <?php if(isset($images) && is_array($images) && !empty($images)): ?>
                                                    <?php foreach ($images as $key => $image) : ?>
                                                        <a class="page--activity-album-inactive"
                                                            href="<?php echo $image ?>" data-fancybox="album-<?php echo get_the_ID() ?>"
                                                            data-type="image">
                                                            <img src="<?php echo $image ?>" />
                                                        </a>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            <?php endif ?>
                                    </div>
                                </div>
                                <?php endwhile ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php get_footer() ?>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script>
    var $ = jQuery.noConflict();
    // Each Album must have unibox data-fancybox attr name
    $('[data-fancybox*="album"]').fancybox({
        infobar: false,
        thumbs: {
            autoStart: true,
            axis: 'x'
        },
        buttons: [
            "close"
        ],
    });
</script>