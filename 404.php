<?php
    global $header_vars; 
    $header_vars = ['header_classes' => ' header--container-white'];
    get_header();
?>
<main class="pad-distance-between-header-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 page--title-section">
                <h1 class="text-center"><span style="color: #e52b50; font-size: 80px">404</span></h1>
                <h2 class="text-center"><?php _e('PAGE NOT FOUND', 'inone') ?></h2>
                <a class="load-more-button formPopupOpener mt-4" href="<?php echo home_url('/') ?>"><?php _e('back to home', 'inone') ?></a>
            </div>
        </div>
    </div>
</main>
<?php get_footer() ?>