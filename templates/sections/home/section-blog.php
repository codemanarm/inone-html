<?php 
    $blogs = get_posts(
        array(
            'numberposts' => 3,
            'orderby'=> 'date',
            'order'=> 'DESC',
            'suppress_filters' => 0
        )
    );

?>

<?php if ( $blogs ) : $post_count = 1; ?>
<section id="artuyt-section-five" class="section ">
    <div class="artuyt-section d-flex padding-60 background-light align-items-center flex-column justify-content-center">
        <h1 class="blog-section-title text-uppercase mt-5 mb-5"><?php _e('Blog', 'codeman') ?></h1>
        <div class="homepage-blog-container d-flex flex-column align-items-center">
            <div class="d-flex align-items-center mb-6 flex-wrap-md justify-content-between-md">
                <?php foreach ( $blogs  as $key => $blog ) :

                    if($key == 0): 
                ?>
                    <div class="col-12 col-xl-7 d-flex align-items-center bg-white no-padding mr-40 blog-item-fluid flex-wrap-md">
                        <div class="col-xl-5 col-md-6 col-12">
                            <span class="fontSegoeUI text_small_light"><?php echo get_the_date('d.m.Y') ?></span>
                            <h2><?php echo get_the_title($blog->ID) ?></h2>
                            <!-- <p> -->
                                <?php echo get_excerpt(200, $blog->ID); ?>
                            <!-- </p> -->
                            <a href="<?php echo get_the_permalink($blog->ID) ?>" class="fw500 btn-custom btn-custom_read_more  text-uppercase no-border align-self-start pl-0 d-inline-block color-dark"><?php _e('read more', 'codeman') ?></a>
                        </div>
                        <div class="col-xl-7 col-md-6 col-12 blog-image blog-image-xl no-padding">
                            <?php 
                                if ( has_post_thumbnail($blog->ID) ) {
                                    echo get_the_post_thumbnail($blog->ID, 'medium', ['class'=>'image_reset']);
                                }
                            ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="d-flex flex-column bg-white blog-item-md no-padding mr-40">
                        <div class="blog-image mb-3">
                            <?php 
                                if ( has_post_thumbnail($blog->ID) ) {
                                    echo get_the_post_thumbnail($blog->ID, 'medium', ['class'=>'image_reset']);
                                }
                            ?>
                        </div>
                        <div class="padding-20">
                            <span class="fontSegoeUI text_small_light">
                                <?php echo get_the_date('d.m.Y') ?>
                            </span>
                            <span class="mb-3 blog-md-title fontSegoeUI d-block">
                                <?php echo get_the_title($blog->ID) ?>
                            </span>
                            <?php echo get_excerpt(200, $blog->ID); ?>
                            <a href="<?php echo get_the_permalink($blog->ID) ?>"
                                class="btn-custom btn-custom_read_more color-dark text-uppercase no-border align-self-start pl-0 fw500 d-inline-block mb-3"><?php _e('read more', 'codeman') ?></a>
                        </div>
                    </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <a href="<?php echo home_url('blog') ?>" class="btn-custom color-dark text-uppercase align-self-start fw500 margin-atuo d-inline-block mb-5"><?php _e('read all blogs', 'codeman') ?></a>
            
        </div>
    </div>
</section>
<?php wp_reset_query() ?>
<?php endif; ?>