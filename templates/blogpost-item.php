<div class="col-lg-4 col-md-6 col-12 mb-36"  data-sal="slide-left" data-sal-delay="700">
    <a href="<?php the_permalink() ?>" class="blog--item box-shadow h-100 ">
        <span class="blog--release-date--tooltip"><?php echo get_the_date( 'M d', get_the_ID() ); ?></span>
        <?php 
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('large', ['class'=>'blog_cover mb-4']);
        } ?>
        <span class="blog--item-description">
            <h3 class="blog--item-title"><?php the_title() ?></h3>
            <p class="blog--item-short-detail">
            	<?php echo get_excerpt(200, get_the_ID()) ?>
            </p>
        </span>
    </a>
</div>