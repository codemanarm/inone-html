<?php $partners = new WP_Query( array(
    'post_type' => 'partners',
    'orderby'=> 'order',
    'order'=> 'ASC',
    'suppress_filters' => 0,
    'posts_per_page' => -1
) );

$count_partners = $partners->found_posts;
$partners_key = 0; ?>
<?php if ( $partners->have_posts() ) : ?>
    <div class="swiper-container partner--section-slider mb-30">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <?php while ( $partners->have_posts() ) : $partners->the_post() ?>
                    <?php 
                    	$url = get_post_meta( get_the_ID(), 'partner_url', true );
                    	$url = $url != '' ? $url : 'javascript:void(0)';
                    	$target = $url != 'javascript:void(0)' ? 'target="_blank"' : '';
                    ?>
                    <?php ++$partners_key;  ?>
                    <a href="<?php echo $url ?>" class="partner--section-slide-item" <?php echo $target ?>>
                        <?php 
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail('thumbnail', ['class'=> 'text-center']);
                        } ?>
                    </a>
                    <?php if($partners_key%15 == 0 && $count_partners > $partners_key): ?>
                    </div>
                    <div class="swiper-slide">
                    <?php endif ?>
                    
                <?php  endwhile ?>
            </div>
        </div>
        <div class="swiper-pagination"></div>
    </div>
<?php endif ?>