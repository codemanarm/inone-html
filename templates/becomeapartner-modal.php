<div class="form--container">
    <div class="burger_bar_container active" id="closeIcon">
        <svg id="drawSVG" width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15 30C6.74342 30 0 23.2895 0 15C0 12.7303 0.493421 10.5263 1.48026 8.48684C1.64474 8.19079 2.00658 8.05921 2.30263 8.19079C2.59868 8.35526 2.73026 8.7171 2.59868 9.01316C1.67763 10.8882 1.25 12.8947 1.25 15C1.25 22.5987 7.43421 28.75 15 28.75C17.2368 28.75 19.4737 28.1908 21.4474 27.1382C24.0132 25.7566 26.1184 23.5855 27.4013 20.9539C28.3224 19.0789 28.75 17.0724 28.75 14.9671C28.75 7.36842 22.5658 1.21711 15 1.21711C12.7632 1.25 10.5263 1.80921 8.55263 2.86184C8.25658 3.02632 7.86184 2.89474 7.69737 2.59868C7.56579 2.30263 7.66447 1.90789 7.96053 1.74342C10.1316 0.592105 12.5329 0 15 0C23.2566 0 30 6.74342 30 15C30 17.2697 29.5066 19.4737 28.5197 21.5132C27.1382 24.375 24.8355 26.7763 22.0395 28.2566C19.8684 29.4079 17.4671 30 15 30Z" fill="white"/>
        </svg>
       <div class="bar1 bars"></div>
       <div class="bar2 bars"></div>
       <div class="bar3 bars"></div>
    </div>   
    
    <?php 
        switch (ICL_LANGUAGE_CODE) {
            case 'hy':
                echo do_shortcode('[contact-form-7 id="270" title="Դառնալ Գործընկեր"]');
                break;
            default:
                echo do_shortcode('[contact-form-7 id="207" title="Become a Partner"]');
                break;
        } ?>

</div>
<div class="partner-popup-overlay"></div>