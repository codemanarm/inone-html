<?php //dynamic_sidebar('footer-1') ?>
<?php $site_options = get_option( 'site-options', $default ); 
$address_url = 'https://www.google.com/maps/place/';
if( isset($site_options) && 
    isset($site_options['company_map_longitude']) &&
    isset($site_options['company_map_latitude'])
){

    $address_url .= $site_options['company_map_longitude'].','.$site_options['company_map_latitude'];
}elseif (isset($site_options['company_address'])) {
    $address_url .= $site_options['company_address'];
}
// echo "<pre>";
// var_dump($site_options);
?>
<footer>
    <div class="container">
        <div class="row ml-n4 mr-n4 align-items-center footer--main">
            <div class="col-xl-6 col-12 d-flex justify-content-between ml-n1 mr-n1">
                <div class="footer--logo-container flex-shrink-0">
                    <a href="<?php echo home_url('/') ?>" class="logo-container d-block navbar-brand p-0 m-0 mb-3" data-sal="slide-right" data-sal-delay="300">
                        <span class="brand--logo brand--logo-white d-block"></span>
                    </a>
                    <p class="mb-40" data-sal="slide-right" data-sal-delay="400">
                        <?php
                            if(isset($site_options) && isset($site_options['company_about'])): 
                                echo $site_options['company_about'];
                            endif;
                        ?>
                    </p>
                    <?php if(   isset($site_options) && 
                                isset($site_options['company_socials_group']) &&
                                is_array($site_options['company_socials_group'])
                            ): ?>
                        <div class="footer--social-container d-flex align-items-center">
                            <?php foreach ($site_options['company_socials_group'] as $key => $item) {
                                echo '<a href="'. $item["company_social_url"] .'" class="footer--social_ico" target="_blank" data-sal="slide-left" data-sal-delay="'. (300 + (($key+1)*100)).'">';
                                    echo '<i class="'. $item["company_social_icon"] .'"></i>';
                                echo '</a>';
                            } ?>
                            
                        </div>
                    <?php endif ?>
                </div>
                <div class="additional_information">
                    <?php if(isset($site_options) && isset($site_options['company_address'])): ?>
                        <a href="<?php echo $address_url ?>" class="footer_additional-item footer_additional-location" target="_blank"  data-sal="slide-down" data-sal-delay="700">
                            <span class="footer--additional-icon">
                                <i class="fas fa-map-marker-alt"></i>
                            </span>
                            <span class="footer_additional-desc-container">
                                <span class="footer_additional-desc-title"><?php _e('Address', 'inone') ?></span>
                                <span class="footer_additional-desc">
                                    <?php echo $site_options['company_address']; ?> 
                                </span>
                            </span>
                        </a>
                    <?php endif; ?>
                    <?php if(isset($site_options) && isset($site_options['company_phone'])): ?>
                        <a href="tel:<?php echo $site_options['company_phone']; ?>" class="footer_additional-item footer_additional-phone" data-sal="slide-right" data-sal-delay="600">
                            <span class="footer--additional-icon">
                                <i class="fas fa-phone-alt"></i>
                            </span>
                            <span class="footer_additional-desc-container">
                                <span class="footer_additional-desc-title"><?php _e('Phone', 'inone') ?></span>
                                <span class="footer_additional-desc">
                                    <?php echo $site_options['company_phone']; ?>
                                </span>
                            </span>
                        </a>
                    <?php endif; ?>
                    <?php if(isset($site_options) && isset($site_options['company_email'])): ?>
                        <a href="mailto:<?php echo $site_options['company_email']; ?>" class="footer_additional-item footer_additional-mail" data-sal="slide-up" data-sal-delay="700">
                            <span class="footer--additional-icon">
                                <i class="fas fa-envelope"></i>
                            </span>
                            <span class="footer_additional-desc-container">
                                <span class="footer_additional-desc-title"><?php _e('Email', 'inone') ?></span>
                                <span class="footer_additional-desc">
                                    <?php echo $site_options['company_email']; ?>
                                </span>
                            </span>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer--map-container">
        <?php if(isset($site_options['company_map_longitude']) && $site_options['company_map_latitude']): ?>
            <div class="contact_map" id="contact_mapID" 
            data-longitude="<?php echo $site_options['company_map_longitude'] ?>" 
            data-latitude="<?php echo $site_options['company_map_latitude'] ?>" >
            </div>
        <?php endif ?>
    </div>
    <div class="footer--main-sub">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <p class="m-0">
                    Copyright © 2019 inOne. All Rights Reserved. Web design by <a href="#" class="footer--sub-link">DMS</a>
                </p>
            </div>
        </div>
        
    </div>
</footer>
<div class="burger_overlay"></div>