<?php
    global $header_vars; 
    $header_vars = ['header_classes' => ' header--container-white'];
    get_header();
?>
<main class="pad-distance-between-header-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 page--title-section">
                <?php the_content() ?>
            </div>
        </div>
    </div>
</main>
<?php get_footer() ?>