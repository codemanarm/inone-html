<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/CMB2/CMB2
 */
/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */
if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action( 'cmb2_admin_init', 'myprefix_register_theme_options_metabox' );
/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function myprefix_register_theme_options_metabox() {

	/**
	 * Registers options page menu item and form.
	 */
	$cmb_options = new_cmb2_box( array(
		'id'           => 'company-general-options',
		'title'        => esc_html__( 'Site Options', 'codeman' ),
		'object_types' => array( 'options-page' ),

		/*
		 * The following parameters are specific to the options-page box
		 * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
		 */

		'option_key'      => 'site-options', // The option key and admin menu page slug.
		// 'icon_url'        => 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
		// 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
		// 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
		// 'capability'      => 'manage_options', // Cap required to view options-page.
		// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
		// 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
		// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
		// 'save_button'     => esc_html__( 'Save Theme Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
	) );

	/*
	 * Options fields ids only need
	 * to be unique within this box.
	 * Prefix is not needed.
	 */

	$cmb_options->add_field( array(
		'name' => __( 'About Company', 'codeman' ),
		'desc' => __( 'Enter some small text about company (footer)', 'codeman' ),
		'id'   => 'company_about',
		'type' => 'wysiwyg',
		// 'default' => 'Default Text',
	) );

	$cmb_options->add_field( array(
		'name' => __( 'Address', 'codeman' ),
		'desc' => __( 'Enter the address of your company', 'codeman' ),
		'id'   => 'company_address',
		'type' => 'text',
		// 'default' => 'Default Text',
	) );

	$cmb_options->add_field( array(
		'name' => __( 'Phone', 'codeman' ),
		'desc' => __( 'Enter the phone number of your company', 'codeman' ),
		'id'   => 'company_phone',
		'type' => 'text',
		// 'default' => 'Default Text',
	) );

	$cmb_options->add_field( array(
		'name' => __( 'Email', 'codeman' ),
		'desc' => __( 'Enter the email address of your company', 'codeman' ),
		'id'   => 'company_email',
		'type' => 'text',
		// 'default' => 'Default Text',
	) );


	$cmb_options->add_field( array(
		'name' => __( 'Company Map Longitude', 'codeman' ),
		'desc' => __( 'Enter Company Map Longitude', 'codeman' ),
		'id'   => 'company_map_longitude',
		'type' => 'text',
		// 'default' => 'Default Text',
	) );
	$cmb_options->add_field( array(
		'name' => __( 'Company Map Latitude', 'codeman' ),
		'desc' => __( 'Enter Company Map Latitude', 'codeman' ),
		'id'   => 'company_map_latitude',
		'type' => 'text',
		// 'default' => 'Default Text',
	) );


	$group_socials = $cmb_options->add_field( array(
       'id'          => 'company_socials_group',
       'type'        => 'group',
       'options'     => array(
           'group_title'   => __( 'Social Media', 'codeman' ) . ' {#}', // {#} gets replaced by row number
           'add_button'    => __( 'Add another Editor', 'codeman' ),
           'remove_button' => __( 'Remove Editor', 'codeman' ),
           'sortable'      => true, // beta
       	),
   	) );


   	//* Title
   	$cmb_options->add_group_field( $group_socials, array(
       'name'    => __( 'Icon', 'codeman' ),
       'desc'      => 'Example: fa fa-facebook (Source for other icons find <a href="https://fontawesome.com/icons">here</a>)',
       'id'      => 'company_social_icon',
       'type'    => 'text',
   	) );
   

   	//* Textarea
   	$cmb_options->add_group_field( $group_socials, array(
       'name'    => __( 'URL', 'codeman' ),
       'id'      => 'company_social_url',
       'type'    => 'text_url',
   	) );

}