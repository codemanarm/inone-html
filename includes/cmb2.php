<?php 
	/**
	 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
	 *
	 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
	 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
	 *
	 * @category YourThemeOrPlugin
	 * @package  Demo_CMB2
	 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
	 * @link     https://github.com/CMB2/CMB2
	 */
	/**
	 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
	 */
	if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/cmb2/init.php';
	} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/CMB2/init.php';
	}


	add_action( 'cmb2_admin_init', 'register_for_business_page_metabox' );
	add_action( 'cmb2_admin_init', 'register_team_page_metabox' );
	add_action( 'cmb2_admin_init', 'register_team_member_metabox' );
	add_action( 'cmb2_admin_init', 'register_team_activities_metabox' );
	add_action( 'cmb2_admin_init', 'register_partners_metabox' );
	add_action( 'cmb2_admin_init', 'register_homepage_metabox' );
	/**
	 * Hook in and add a metabox to add fields
	 */

	function register_for_business_page_metabox()
	{
		$prefix = 'page_';

		$cmb = new_cmb2_box( array(
			'id'           => 'business',
			'title'        => 'Page Information',
			'object_types' => array( 'page' ), // post type
			'show_on'      => array( 'key' => 'page-template', 'value' => 'page-for-business.php' ),
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );


		$cmb->add_field( array(
			'name' => esc_html__( 'Page Cover Photo', 'cmb2' ),
			'desc' => esc_html__( 'Select the Page cover photo', 'cmb2' ),
			'id'   => $prefix . 'cover',
			'type' => 'file',
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Section 2 Left Image', 'cmb2' ),
			'desc' => esc_html__( 'Select the left image for row 2', 'cmb2' ),
			'id'   => $prefix . '2_row_image',
			'type' => 'file',
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Section 2 Right Text', 'cmb2' ),
			'desc' => esc_html__( 'Enter the text for row 2', 'cmb2' ),
			'id'   => $prefix . '2_row_text',
			'type' => 'wysiwyg',
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Partners Section Text', 'cmb2' ),
			'desc' => esc_html__( 'Enter the text for partners section', 'cmb2' ),
			'id'   => $prefix . 'partners_section_text',
			'type' => 'wysiwyg',
		) );


		$cmb->add_field( array(
			'name' => esc_html__( 'Become a partner button text', 'cmb2' ),
			'desc' => esc_html__( 'Enter the text for partners section', 'cmb2' ),
			'id'   => $prefix . 'become_a_partner_btn_text',
			'type' => 'text',
		) );
	}

	/**
	 * Hook in and add a metabox to add fields
	 */

	function register_team_page_metabox()
	{
		$prefix = 'page_';

		$cmb = new_cmb2_box( array(
			'id'           => 'meta_team_page',
			'title'        => 'Page Information',
			'object_types' => array( 'page' ), // post type
			'show_on'      => array( 'key' => 'page-template', 'value' => 'page-team.php' ),
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Team Activity Text', 'cmb2' ),
			'desc' => esc_html__( 'Enter the text for team activity', 'cmb2' ),
			'id'   => $prefix . 'team_activity_text',
			'type' => 'wysiwyg',
		) );
	}

		/**
	 * Hook in and add a metabox to add fields
	 */

	function register_team_member_metabox()
	{
		$prefix = 'team_';

		$cmb = new_cmb2_box( array(
			'id'           => $prefix.'member_metabox',
			'title'        => 'Team Member Information',
			'object_types' => array( 'team' ), // post type
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Team member position', 'cmb2' ),
			// 'desc' => esc_html__( 'Enter the text for team activity', 'cmb2' ),
			'id'   => $prefix . 'member_position',
			'type' => 'text',
		) );
	}


	/**
	 * Hook in and add a metabox to add fields
	 */

	function register_team_activities_metabox()
	{
		$prefix = 'team_activity_';

		$cmb = new_cmb2_box( array(
			'id'           => 'meta_team_activity',
			'title'        => 'Activity Information',
			'object_types' => array( 'team_activity' ), // post type
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Activity Images', 'cmb2' ),
			'desc' => esc_html__( 'Select an images', 'cmb2' ),
			'id'   => $prefix . 'images',
			'type' => 'file_list',
		) );
	}

	/**
	 * Hook in and add a metabox to add fields
	 */

	function register_partners_metabox()
	{
		$prefix = 'partner_';

		$cmb = new_cmb2_box( array(
			'id'           => 'partners_metabox',
			'title'        => 'Partner Information',
			'object_types' => array( 'partners' ), // post type
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Partner Website URL', 'cmb2' ),
			'desc' => esc_html__( 'Enter the URL of partner', 'cmb2' ),
			'id'   => $prefix . 'url',
			'type' => 'text_url',
		) );
	}

	/**
	 * Hook in and add a metabox to add fields
	 */

	function register_homepage_metabox()
	{
		$prefix = 'homepage_app_section_';

		$cmb = new_cmb2_box( array(
			'id'           => $prefix.'metabox_id',
			'option_key'    => $prefix.'metabox',
			'title'        => 'Applications Section',
			'object_types' => array( 'page' ), // post type
			'show_on'      => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );


		$cmb->add_field( array(
			'name' => esc_html__( 'Page Cover Photo', 'cmb2' ),
			'desc' => esc_html__( 'Select the Page cover photo', 'cmb2' ),
			'id'   => $prefix . 'cover',
			'type' => 'file',
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Apple Store Image', 'cmb2' ),
			'desc' => esc_html__( 'Choose an image for the Apple Store', 'cmb2' ),
			'id'   => $prefix . 'appstore_image',
			'type' => 'file',
		) );
		$cmb->add_field( array(
			'name' => esc_html__( 'Apple Store application URL', 'cmb2' ),
			'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
			'id'   => $prefix . 'appstore_url',
			'type' => 'text_url',
		) );


		$cmb->add_field( array(
			'name' => esc_html__( 'PlayStore Image', 'cmb2' ),
			'desc' => esc_html__( 'Choose an image for the Apple Store', 'cmb2' ),
			'id'   => $prefix . 'playstore_image',
			'type' => 'file',
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'PlayStore application URL', 'cmb2' ),
			'desc' => esc_html__( 'Enter the URL for the app in the PlayStore', 'cmb2' ),
			'id'   => $prefix . 'playstore_url',
			'type' => 'text_url',
		) );

		$prefix = 'homepage_1_row_';

		$row_1_repeater = $cmb->add_field( array(
	       'id'          => $prefix . 'repeater',
	       'type'        => 'group',
	       'options'     => array(
	           'group_title'   => __( 'Intruduction of Application Tab', 'codeman' ) . ' {#}', // {#} gets replaced by row number
	           'add_button'    => __( 'Add another Tab', 'codeman' ),
	           'remove_button' => __( 'Remove tab', 'codeman' ),
	           'sortable'      => true, // beta
	       ),
	   ) );

		//* Textarea
	    $cmb->add_group_field( $row_1_repeater, array(
	       'name'    => __( 'Tab Name', 'codeman' ),
	       'id'      => $prefix . 'title',
	       'type'    => 'text',
	    ) );

	    //* Title
	    $cmb->add_group_field( $row_1_repeater, array(
	       'name'    => __( 'Screenshot (Image without phone)', 'codeman' ),
	       'id'      => $prefix . 'repeater_image',
	       'type'    => 'file',
	    ) );

	    //* Textarea
	    $cmb->add_group_field( $row_1_repeater, array(
	       'name'    => __( 'Text', 'codeman' ),
	       'id'      => $prefix . 'repeater_text',
	       'type'    => 'wysiwyg',
	    ) );

		$prefix = 'homepage_2_row_';

		$cmb = new_cmb2_box( array(
			'id'           => $prefix.'metabox_id',
			'option_key'    => $prefix.'metabox',
			'title'        => 'Section N.2',
			'object_types' => array( 'page' ), // post type
			'show_on'      => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
			'context'      => 'normal', //  'normal', 'advanced', or 'side'
			'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
			'show_names'   => true, // Show field names on the left
		) );

		$cmb->add_field( array(
			'name' => esc_html__( 'Section Left Image', 'cmb2' ),
			'desc' => esc_html__( 'Choose an image ', 'cmb2' ),
			'id'   => $prefix . 'image',
			'type' => 'file',
		) );
		$cmb->add_field( array(
			'name' => esc_html__( 'Section Content/Title', 'cmb2' ),
			// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
			'id'   => $prefix . 'content',
			'type' => 'wysiwyg',
		) );


		$row_2_repeater = $cmb->add_field( array(
	       'id'          => $prefix . 'repeater',
	       'type'        => 'group',
	       'options'     => array(
	           'group_title'   => __( 'Icon-Text Repeater', 'codeman' ) . ' {#}', // {#} gets replaced by row number
	           'add_button'    => __( 'Add another Row', 'codeman' ),
	           'remove_button' => __( 'Remove Row', 'codeman' ),
	           'sortable'      => true, // beta
	       ),
	   ) );


	   //* Title
	   $cmb->add_group_field( $row_2_repeater, array(
	       'name'    => __( 'Icon', 'codeman' ),
	       'id'      => $prefix . 'repeater_icon',
	       'type'    => 'file',
	   ) );

	   //* Textarea
	   $cmb->add_group_field( $row_2_repeater, array(
	       'name'    => __( 'Text', 'codeman' ),
	       'id'      => $prefix . 'repeater_text',
	       'type'    => 'textarea',
	   ) );



	   $prefix = 'homepage_3_row_';

	   $cmb = new_cmb2_box( array(
	   	'id'           => $prefix.'metabox_id',
	   	'option_key'    => $prefix.'metabox',
	   	'title'        => 'Section N.3',
	   	'object_types' => array( 'page' ), // post type
	   	'show_on'      => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
	   	'context'      => 'normal', //  'normal', 'advanced', or 'side'
	   	'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	   	'show_names'   => true, // Show field names on the left
	   ) );

	   $cmb->add_field( array(
	   	'name' => esc_html__( 'Section Left Content', 'cmb2' ),
	   	// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   	'id'   => $prefix . 'left_content',
	   	'type' => 'wysiwyg',

	   ) );

	   $cmb->add_field( array(
	   	'name' => esc_html__( 'Section Center Image', 'cmb2' ),
	   	'desc' => esc_html__( 'Choose an image ', 'cmb2' ),
	   	'id'   => $prefix . '_image',
	   	'type' => 'file',
	   ) );

	   $cmb->add_field( array(
	   	'name' => esc_html__( 'Section Right Content', 'cmb2' ),
	   	// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   	'id'   => $prefix . 'right_content',
	   	'type' => 'wysiwyg',
	   ) );


	   $prefix = 'homepage_4_row_';

	   $cmb = new_cmb2_box( array(
	   	'id'           => $prefix.'metabox_id',
	   	'option_key'    => $prefix.'metabox',
	   	'title'        => 'Section N.4',
	   	'object_types' => array( 'page' ), // post type
	   	'show_on'      => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
	   	'context'      => 'normal', //  'normal', 'advanced', or 'side'
	   	'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	   	'show_names'   => true, // Show field names on the left
	   ) );

	   $cmb->add_field( array(
	   	'name' => esc_html__( 'Section Left Image', 'cmb2' ),
	   	// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   	'id'   => $prefix . 'phone_image',
	   	'type' => 'file',

	   ) );

	   $cmb->add_field( array(
	   	'name' => esc_html__( 'Section Center Image', 'cmb2' ),
	   	'desc' => esc_html__( 'Choose an image ', 'cmb2' ),
	   	'id'   => $prefix . 'image',
	   	'type' => 'file',
	   ) );

	   $cmb->add_field( array(
	   	'name' => esc_html__( 'Section Content', 'cmb2' ),
	   	// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   	'id'   => $prefix . 'content',
	   	'type' => 'wysiwyg',
	   ) );

	   $prefix = 'homepage_partners_row_';

	   $cmb = new_cmb2_box( array(
	   	'id'           => $prefix.'metabox_id',
	   	'option_key'    => $prefix.'metabox',
	   	'title'        => 'Section Partners',
	   	'object_types' => array( 'page' ), // post type
	   	'show_on'      => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
	   	'context'      => 'normal', //  'normal', 'advanced', or 'side'
	   	'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	   	'show_names'   => true, // Show field names on the left
	   ) );

	   	$cmb->add_field( array(
	   	'name' => esc_html__( 'Section Content', 'cmb2' ),
	   	// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   	'id'   => $prefix . 'content',
	   	'type' => 'wysiwyg',
	   ) );

	   	$prefix = 'homepage_6_row_';

	   	$cmb = new_cmb2_box( array(
	   		'id'           => $prefix.'metabox_id',
	   		'option_key'    => $prefix.'metabox',
	   		'title'        => 'Last Section (Row 6)',
	   		'object_types' => array( 'page' ), // post type
	   		'show_on'      => array( 'key' => 'page-template', 'value' => 'front-page.php' ),
	   		'context'      => 'normal', //  'normal', 'advanced', or 'side'
	   		'priority'     => 'high',  //  'high', 'core', 'default' or 'low'
	   		'show_names'   => true, // Show field names on the left
	   	) );

	   	$cmb->add_field( array(
	   		'name' => esc_html__( 'Section Content', 'cmb2' ),
	   		// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   		'id'   => $prefix . 'content',
	   		'type' => 'wysiwyg',
	   	) );

	   	$cmb->add_field( array(
	   		'name' => esc_html__( 'Section Content', 'cmb2' ),
	   		// 'desc' => esc_html__( 'Enter the URL for the app in the Apple Store', 'cmb2' ),
	   		'id'   => $prefix . 'image',
	   		'type' => 'file',
	   	) );


	}
?>