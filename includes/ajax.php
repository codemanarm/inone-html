<?php
	function ajax_login()
	{
		$is_auth = wp_signon(
			array(
				'user_login' => $_POST['user_login'],
				'user_password' => $_POST['user_password'],
				'remember' => $_POST['remember']
			)
		);
		if($is_auth->ID){
			echo json_encode(array('status'=>'success', 'redirect' => site_url('/shop')));
		}else{
			echo json_encode(array('status'=>'failed', 'message' => __('Email or password is incorrect.', 'codeman')));
		}
		exit();die();
	}
	add_action( 'wp_ajax_nopriv_ajax_login', 'ajax_login' );
	add_action( 'wp_ajax_ajax_login', 'ajax_login' );

	function ajax_register()
	{
		$errors = array();
		if(!$_POST['first_name'] || $_POST['first_name'] == ''){
			$errors['fields']['first_name'] = __('This filed is required');
		}
		if(!$_POST['last_name'] || $_POST['last_name'] == ''){
			$errors['fields']['last_name'] = __('This filed is required');
		}
		if(!$_POST['user_email'] || $_POST['user_email'] == ''){
			$errors['fields']['user_email'] = __('This filed is required');
		}
		if(!$_POST['user_pass'] || $_POST['user_pass'] == ''){
			$errors['fields']['user_pass'] = __('This filed is required');
		}
		if(!$_POST['user_pass2'] || $_POST['user_pass2'] != $_POST['user_pass']  ){
			$errors['fields']['user_pass2'] = __('The passwords do not match');
		}
		if(!$_POST['sex'] || $_POST['sex'] == false){
			$errors['fields']['sex'] = __('This filed is required');
		}
		if(!$_POST['terms_and_policy'] || $_POST['terms_and_policy'] == false || $_POST['terms_and_policy'] == 'false'){
			$errors['fields']['terms_and_policy'] = __('This filed is required');
		}
		if(isset($errors['fields'])){
			echo json_encode(array('status' => 'required_fields', 'errors' => $errors['fields'] ));
			exit(); die();
		}
		$user = wp_insert_user(
			array(
				'first_name' => $_POST['first_name'],
				'last_name' => $_POST['last_name'],
				'user_email' => $_POST['user_email'],
				'user_login' => $_POST['user_email'],
				'user_pass' => $_POST['user_pass'],
				'role' => 'customer',
			)
		);
		
		// var_dump($user);exit();
		if($user->errors && $user->errors['existing_user_login']){
			echo json_encode(array(
				'status' => 'email_exist',
				'message' => __('Looks like you already have an account with this email address!', 'codeman')
			));
		}else{
			auto_login($user);
			echo json_encode(array(
				'status' => 'success',
				'redirect' => site_url('/shop'),
				'message' => __('You are successfully registered your account.', 'codeman')
			));
		}
		exit(); die();
	}
	add_action( 'wp_ajax_nopriv_ajax_register', 'ajax_register' );
	add_action( 'wp_ajax_ajax_register', 'ajax_register' );

	function auto_login($user_id) {
	    if (!is_user_logged_in()) {
	        
	        //login
	        wp_set_current_user($user_id);
	        wp_set_auth_cookie($user_id);
	        // do_action('wp_login', $user_login);
	    }
	}
?>