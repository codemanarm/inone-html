<?php /* Template Name: For Business */ ?>
<?php 
    global $header_vars; 
    $header_vars = ['brand_logo_classes' => 'brand--logo-white'];

    $top_cover = get_post_meta( get_the_ID(), 'page_cover', true );
    $row_2_image = get_post_meta( get_the_ID(), 'page_2_row_image', true );
    $row_2_content = get_post_meta( get_the_ID(), 'page_2_row_text', true );
    $partners_section_text = get_post_meta( get_the_ID(), 'page_partners_section_text', true );
    $partners_btn_text = get_post_meta( get_the_ID(), 'page_become_a_partner_btn_text', true );
?>

<?php get_header() ?>
<main class="business--page pb-100">
    <div class="subheader-section mb-100">
        <img src="<?php echo $top_cover ?>" alt="<?php echo get_the_title() ?>">
        <div class="container" >
            <div class="row flex-column subheader--title-white"  data-sal="slide-right" data-sal-delay="700">
                <?php the_content() ?>
            </div>
        </div>
    </div>
    <div class="image-with-content-box-container d-flex" data-sal="slide-right">
        <img src="<?php echo $row_2_image ?>" alt="<?php echo get_the_title() ?>-row-2"  data-sal-delay="400">
        <div class="image-with-content-box-description" data-sal="slide-left" data-sal-delay="500">
            <?php echo apply_filters('the_content', $row_2_content); ?>
        </div>
    </div>
    <div class="container" data-sal="slide-top" data-sal-delay="500">
        <div class="row">
            <div class="partner--section-container w-100">
                <div class="col-12 page--title-section">
                    
                    <?php echo $partners_section_text ?>
                </div>
                <?php get_template_part( 'templates/partners', 'slider' ); ?>
                <a id="formPopupOpener" href="#" class="load-more-button formPopupOpener"  data-sal="slide-right" data-sal-delay="700"><?php _e($partners_btn_text, 'inone')?></a>       
            </div>
        </div>
    </div>
</main>
<?php get_template_part( 'templates/becomeapartner', 'modal' ); ?>
<?php get_footer() ?>