<?php get_template_part('templates/footer') ?>    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-OYxseiIXU-KzryxME4LtqPNICx8jYZU&callback=initMap"></script>
    <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/sal.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/main.js"></script>
    
    <script>
        function initMap() {
	        let mapContainer = jQuery('#contact_mapID'); 
	        let longitude = mapContainer.data('longitude');
	        let latitude = mapContainer.data('latitude');

        	console.log(longitude);
        	console.log(latitude);
            // The location of Uluru
            var uluru = { lat: latitude, lng: longitude };
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('contact_mapID'), { zoom: 14, center: uluru }
            );
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({ position: uluru, map: map });
        }
    </script>
    <script>
        var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 10,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
        breakpoints:{
            768:{
                // slidesPerView: 5,
                spaceBetween: 38,
            }
        }
        });
        const scrollAnimations = sal({
            threshold: 0.1,
        });

        const box = document.querySelector('.home--page-about-project-phone');
        if(box){
        	const mouse = new Mouse(box);
        }

        window.addEventListener('resize', () => {
            window.innerWidth < 768 && BurgerAndMenuContentOpenClose();
        });

        window.innerWidth < 768 && BurgerAndMenuContentOpenClose();
    </script>
    <script>
      	// Popup Form
          popupOpener && openPopupForm();
    </script>
    <?php wp_footer() ?> 
</body>
</html>