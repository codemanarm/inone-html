<?php /* Template Name: Home Template */ ?>

<?php 
global $header_vars; 
$header_vars = ['brand_logo_classes' => 'brand--logo-white'];
$pageID = get_option('page_on_front');
// var_dump(get_the_ID(), $pageID);
$page_meta = get_post_meta( get_the_ID() );
$row_1_repeater = get_post_meta(get_the_ID(), 'homepage_1_row_repeater', true);
$row_2_repeater = get_post_meta(get_the_ID(), 'homepage_2_row_repeater', true);
// echo "<pre>";
// var_dump($row_1_repeater);
// exit();

get_header();

?>
    <main class="home--page">
        <div class="subheader-section mb-100">
            <?php if(isset($page_meta['homepage_app_section_cover'])): ?>
                <img src="<?php echo $page_meta['homepage_app_section_cover'][0] ?>" alt="page cover image">
            <?php endif ?>
            <div class="container">
                <div class="row flex-column subheader--title-white"   data-sal="slide-right" data-sal-delay="700">
                    <?php the_content() ?>
                </div>
                <div class="row flex-column">
                    <div class="app--buttons d-flex align-items-center" data-sal="slide-left" data-sal-delay="1000">
                        <?php if(isset($page_meta['homepage_app_section_appstore_image']) &&
                                isset($page_meta['homepage_app_section_appstore_url'])): ?>
                            <a href="<?php echo $page_meta['homepage_app_section_appstore_url'][0] ?>" class="app--button app--button-istore mr-3">
                                <img src="<?php echo $page_meta['homepage_app_section_appstore_image'][0] ?>" alt="apple store">
                            </a>
                        <?php endif ?>
                        <?php if(isset($page_meta['homepage_app_section_playstore_image']) &&
                                isset($page_meta['homepage_app_section_playstore_url'])): ?>
                            <a href="<?php echo $page_meta['homepage_app_section_playstore_url'][0] ?>" class="app--button app--button-android">
                              <img src="<?php echo get_template_directory_uri() ?>/assets/android.png" alt="playstore">
                            </a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if(isset($row_1_repeater) && !empty($row_1_repeater)): ?>
        <div class="container">
            <div class="row home--page-about-project">
                <div class="home--page-about-project-dec" data-sal="slide-right" data-sal-delay="300">
                    <div class="home--about-project-item-wrapper">
                        <?php foreach ($row_1_repeater as $key => $item) { ?>
                            <div class="home--about-project-item <?php echo $key == 0 ? 'active' : '' ?>" data-page-select="<?php echo $item['homepage_1_row_title'] ?>">
                                <?php echo apply_filters('the_content', $item['homepage_1_row_repeater_text']) ?>
                            </div>
                        <?php } ?>
                    </div>

                </div>
                <div class="d-flex home--page-about-project-description" data-sal="slide-left" data-sal-delay="300">
                    <div class="home--page-about-project-cards">
                        <div class="home--page-about-card-item-dropdown">
                            <?php if(isset($row_1_repeater[1]) ): ?>
                            <div class="home--page-about-card-item mb-0 " data-page-select="<?php echo $row_1_repeater[1]['homepage_1_row_title'] ?>">
                                <i class="home--page-about-card-item-icon card-item-icon-bank-card"></i>
                                <h3 class="home--page-about-card-item-title">
                                    <?php echo $row_1_repeater[1]['homepage_1_row_title'] ?>
                                </h3>
                            </div>
                            <?php endif ?>
                            <?php if(isset($row_1_repeater[2]) ): ?>
                            <div class="home--page-about-card-item home--page-about-card-subitem" data-page-select="<?php echo $row_1_repeater[2]['homepage_1_row_title'] ?>">
                                <i class="home--page-about-card-item-icon card-item-icon-virtual-card"></i>
                                <h3 class="home--page-about-card-item-title">
                                    <?php echo $row_1_repeater[2]['homepage_1_row_title'] ?>
                                </h3>
                            </div>
                            <?php endif ?>
                        </div>
                        <?php if(isset($row_1_repeater[3]) ): ?>
                            <div class="home--page-about-card-item" data-page-select="<?php echo $row_1_repeater[3]['homepage_1_row_title'] ?>">
                                <i class="home--page-about-card-item-icon card-item-icon-coupons"></i>
                                <h3 class="home--page-about-card-item-title">
                                    <?php echo $row_1_repeater[3]['homepage_1_row_title'] ?>
                                </h3>
                            </div>
                        <?php endif ?>
                        <?php if(isset($row_1_repeater[4]) ): ?>
                            <div class="home--page-about-card-item" data-page-select="<?php echo $row_1_repeater[4]['homepage_1_row_title'] ?>">
                                <i class="home--page-about-card-item-icon card-item-icon-notification"></i>
                                <h3 class="home--page-about-card-item-title">
                                    <?php echo $row_1_repeater[4]['homepage_1_row_title'] ?>
                                </h3>
                            </div>
                        <?php endif ?>
                    </div>                    
                    <div class="home--page-about-project-phone">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/phone.png" class="home--phone-layer">
                        <?php foreach ($row_1_repeater as $key => $item) { ?>
                            <img src="<?php echo $item['homepage_1_row_repeater_image'] ?>"  class="home--phone-image-inner <?php echo $key == 0 ? 'active' : '' ?>" data-page-select="<?php echo $item['homepage_1_row_title'] ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>
        <?php if(false): ?>
            <div class="image-with-content-box-container d-flex home--page-digital-card-product">
                <?php if(isset($page_meta['homepage_2_row_image']) && isset($page_meta['homepage_2_row_image'])): ?>
                    <img src="<?php echo $page_meta['homepage_2_row_image'][0] ?>" alt="image-with-content-box-cover" data-sal="slide-right" data-sal-delay="300">
                <?php endif ?>
                <div class="home--digital-card-box" data-sal="fade" data-sal-delay="300">
                    <?php if(isset($page_meta['homepage_2_row_content']) &&
                            isset($page_meta['homepage_2_row_content'])): 
                            echo apply_filters('the_content', $page_meta['homepage_2_row_content'][0]);
                        endif;
                    ?>

                    <?php if(!empty($row_2_repeater)): ?>
                        <?php foreach ($row_2_repeater as $key => $item) { ?>
                            <div class="home--digital-card-list-item d-flex align-items-center">
                                <i class="home--digital-card-list-icon" style="background-image: url('<?php echo $item['homepage_2_row_repeater_icon'] ?>')"></i>
                                <h3 class="home--digital-card-list-title mb-0"><?php echo $item['homepage_2_row_repeater_text'] ?></h3>
                            </div>
                        <?php } ?>
                    <?php endif ?>
                 </div>
            </div>
        <?php endif ?>
        <div class="container home--page-exlusive-card-wrapper">
            <div class="row">
                <div class="home--page-exlusive-card-container d-flex align-items-center justify-content-between">
                    <div class="home--exclusive-content col-xl-3 col-12" data-sal="zoom-in" data-sal-delay="300">
                        <?php if(isset($page_meta['homepage_3_row_left_content'])):
                            echo apply_filters('the_content', $page_meta['homepage_3_row_left_content'][0]);
                        endif ?>
                    </div>
                    <?php if(isset($page_meta['homepage_3_row__image'])): ?>
                        <div class="home--exclusive-phone col-xl-5 col-md-6" data-sal="zoom-in" data-sal-delay="600" style="background-image: url('<?php echo $page_meta['homepage_3_row__image'][0] ?>')">
                        </div>
                    <?php endif ?>
                    <ul class="home--exclusive-list col-xl-3 col-md-6" data-sal="zoom-in" data-sal-delay="900">
                        <?php if(isset($page_meta['homepage_3_row_right_content'])):
                            echo apply_filters('the_content', $page_meta['homepage_3_row_right_content'][0]);
                        endif ?>
                    </ul>
                </div>
                <div class="home--page-ticket-container">
                    <?php if(isset($page_meta['homepage_4_row_phone_image'])): ?>
                        <div class="home--ticket-phone" data-sal="slide-left" data-sal-delay="300"
                        style="background-image: url('<?php echo $page_meta['homepage_4_row_phone_image'][0] ?>')"></div>
                    <?php endif ?>
                    
                    <div class="home--ticket d-flex align-items-center justify-content-between" data-sal="slide-right" data-sal-delay="300">
                        <?php if(isset($page_meta['homepage_4_row_image'])): ?>
                            <img src="<?php echo $page_meta['homepage_4_row_image'][0] ?>" alt="" class="home--ticket-image">
                        <?php endif ?>
                        
                        <div class="home--ticket-content">
                            <?php if(isset($page_meta['homepage_4_row_content'])): ?>
                               <?php echo apply_filters('the_content', $page_meta['homepage_4_row_content'][0]) ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="partner--section-container w-100" data-sal="fade" data-sal-delay="300">
                    <div class="col-12 page--title-section">
                        <?php if(isset($page_meta['homepage_partners_row_content'])): ?>
                           <?php echo apply_filters('the_content', $page_meta['homepage_partners_row_content'][0]) ?>
                        <?php endif ?>
                    </div>
                    <?php get_template_part( 'templates/partners', 'slider' ); ?>
                    <a id="formPopupOpener" href="#" class="load-more-button formPopupOpener"><?php _e('become a partner', 'inone')</a>       
                </div>
                <div class="home-page--promo-section w-100" data-sal="slide-up" data-sal-delay="300" >
                    <?php if(isset($page_meta['homepage_6_row_content'])): ?>
                        <?php echo apply_filters('the_content', $page_meta['homepage_6_row_content'][0]) ?>
                    <?php endif ?>
                    <div class="app--buttons app--buttons-dark d-flex align-items-center justify-content-center">
                        <?php if(isset($page_meta['homepage_app_section_appstore_image']) &&
                                isset($page_meta['homepage_app_section_appstore_url'])): ?>
                            <a href="<?php echo $page_meta['homepage_app_section_appstore_url'][0] ?>" class="app--button app--button-istore mr-3">
                                <img src="<?php echo $page_meta['homepage_app_section_appstore_image'][0] ?>" alt="apple store">
                            </a>
                        <?php endif ?>
                        <?php if(isset($page_meta['homepage_app_section_playstore_image']) &&
                                isset($page_meta['homepage_app_section_playstore_url'])): ?>
                            <a href="<?php echo $page_meta['homepage_app_section_playstore_url'][0] ?>" class="app--button app--button-android">
                              <img src="<?php echo get_template_directory_uri() ?>/assets/android.png" alt="playstore">
                            </a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if(isset($page_meta['homepage_6_row_image'])): ?>
            <img src="<?php echo $page_meta['homepage_6_row_image'][0] ?>" class="home-page--promo-section-cover">
        <?php endif ?>
    </main>
    <?php get_template_part( 'templates/becomeapartner', 'modal' ); ?>
<?php get_footer() ?>