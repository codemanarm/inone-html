<?php

global $header_vars; 
$header_vars = ['header_classes' => ' header--container-white'];
$related_posts = new WP_Query( array(
    'post_type' => 'post',
    'orderby'=> 'date',
    'order'=> 'DESC',
    'suppress_filters' => 0,
    'post__not_in' => array(get_the_ID()),
    'posts_per_page' => 4,
) );
get_header();

if ( have_posts() ):
	the_post();
?>
<main class="pad-distance-between-header-footer">
    <div class="container">
        <div class="row flex-column">
            <span class="blog--release-date" data-sal="slide-down" data-sal-delay="1200"><?php echo get_the_date( 'M d', get_the_ID() ); ?></span>
            <h2 class="blog--inner-title text-center page--title" data-sal="slide-left" data-sal-delay="1000" data-sal-easing="ease-out-back"><?php the_title() ?></h2>
            <?php 
            if ( has_post_thumbnail() ) {
                the_post_thumbnail('full', ['class'=>'blog--inner-cover', 'data-sal'=>"slide-right", "data-sal-delay"=>"700"]);
            } ?>
            <div class="blog--inner-content">
                <div class="blog--inner-description" data-sal="slide-up" data-sal-delay="300">
                    <?php the_content() ?>
                </div>
                <div class="social--share-container mb-5">
                    <p class="social--text"><?php _e('Share:', 'inone') ?></p>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=" class="social_ico" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a   href="https://twitter.com/intent/tweet?text=Hello%20world&url=https://help.twitter.com/en/using-twitter/add-twitter-share-button&" class="social_ico twitter-share-button" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="" class="social_ico" target="_blank">
                        <i class="fab fa-google"></i>
                    </a>
                </div>
                <div class="related--items-container">
                    <?php if ( $related_posts->have_posts() ) : ?>
                        <h4 class="related--items-container-title mb-20"><?php _e('Related news', 'inone') ?></h4>
                        <?php while ( $related_posts->have_posts() ) : $related_posts->the_post() ?>

                            <a href="<?php the_permalink() ?>">
                                <div class="related--item-single box-shadow">
                                    <span class="blog--release-date--tooltip"><?php echo get_the_date( 'M d', get_the_ID() ); ?></span>
                                    <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('medium', ['class'=>'related--item-single-cover']);
                                    } ?>

                                    <div class="related--item-contet">
                                        <h5 class="related--item-title"><?php the_title() ?></h5>
                                        <p>
                                            <?php echo get_excerpt(150, get_the_ID()) ?>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        <?php endwhile; ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</main>
<?php endif; ?>
<?php get_footer() ?>