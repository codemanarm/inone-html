<?php
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'generator');

	// wp_enqueue_script( 'ajax-request', get_template_directory_uri().'/public/javascript/ajax.js', array( 'jquery' ) );
	// wp_localize_script( 'ajax-request', 'app', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'widgets' );
	/**
	 * Register Custom Navigation Walker
	 */
	function require_includes(){
		require_once get_template_directory() . '/includes/wp-bootstrap-navwalker.php';
		require_once get_template_directory() . '/includes/cmb2.php';
		require_once get_template_directory() . '/includes/cmb2-admin.php';
		// require_once get_template_directory() . '/includes/ajax.php';
	}
	add_action( 'after_setup_theme', 'require_includes' );

	// Registering Site Menu's
	register_nav_menus( array(
	    'primary' => __( 'Primary Menu', 'codeman' ),
	    'booter' => __( 'Footer Menu', 'codeman' ),
	) );

	// Making an excerpt of some content
	function get_excerpt($num = 200, $post_id = null){
		if($post_id){
			$excerpt = get_the_content(null, false, $post_id);
		}else{
			$excerpt = get_the_content();
		}

		$excerpt = preg_replace(" ([.*?])",'',$excerpt);
		$excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, $num);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
		$excerpt = $excerpt.'...';
		return $excerpt;
	}

	// Replacing WP logo with Codeman's
	function codeman_logo() { ?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/codemanss-logo-colored.svg);
	        height:65px;
	        width:300px;
	        background-size: 300px 65px;
	        background-repeat: no-repeat;
	        padding-bottom: 0px;
	        }
	    </style>
	<?php }
	add_action( 'login_enqueue_scripts', 'codeman_logo' );

 	//// Registering Custom Post Types	
	function codeman_custom_post_type()
	{
	    register_post_type('partners',
           array(
               'labels'      => array(
                   'name'          => __('Partners'),
                   'singular_name' => __('Partner'),
               ),
               'public'      => true,
               // 'has_archive' => true,
               'supports' => array('title', 'thumbnail', ),
               'rewrite'     => array( 'slug' => 'partners' ), // my custom slug
           )
	    );

	    register_post_type('team',
           array(
               'labels'      => array(
                   'name'          => __('Team'),
                   'singular_name' => __('Team'),
               ),
               'public'      => true,
               // 'has_archive' => true,
               'supports' => array('title', 'thumbnail', ),
               'rewrite'     => array( 'slug' => 'team' ), // my custom slug
           )
	    );


	    register_post_type('team_activity',
           array(
               'labels'      => array(
                   'name'          => __('Team Activities'),
                   'singular_name' => __('Team Activity'),
               ),
               'public'      => true,
               // 'has_archive' => true,
               'supports' => array('title', 'thumbnail',),
               'rewrite'     => array( 'slug' => 'team_activity' ), // my custom slug
           )
	    );
	}
	add_action('init', 'codeman_custom_post_type');
	//// End Registering Of Custom Post Types

	//// Registering Sidebars
	if ( function_exists('register_sidebar') ){
		register_sidebar(array(
			'name' => 'Footer 1',	
			'id' => 'footer-1',		
			'before_widget' => '<div class="footer-nav-col no-padding col-md-4 col-lg-2 tablet-margin">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="color-grey-2 text-uppercase">',
			'after_title' => '</h5>',
			)
		);
	}
	//// End Registering Sidebars


	// Making a Language switcher
	function language_switcher(){
	    // uncomment this to find your theme's menu location
	    //echo "args: <pre>"; print_r($args); echo "</pre>";
	 
	    // add $args->theme_location == 'primary-menu' in the conditional if we want to specify the menu location.
	    // if (function_exists('icl_get_languages') && $args->theme_location == 'horizontal-menu') {
	        $languages = icl_get_languages('skip_missing=0');
	 
	        if(!empty($languages)){
	 
	            foreach($languages as $l){
	                if(!$l['active']){
	                    // flag with native name
	                    $items = $items. '<a href="' . $l['url'] . '" class="nav-link header--nav-link language_bar">' . $l['translated_name'] . '</a>';
	                    //only flag
	                    //$items = $items . '<li class="menu-item menu-item-language"><a href="' . $l['url'] . '"><img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="18" /></a></li>';
	                }
	            }
	        }
	    // }
	 
	    return $items;
	}

	function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

?>