function map(x, in_min, in_max, out_min, out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

class Mouse {
   constructor(el) {
      
        this.x = 0;
        this.y = 0;

        this.centerX = el.clientWidth / 2;
        this.centerY = el.clientHeight / 2;

        el.onmousemove = e => {
            let rect = el.getBoundingClientRect();
            this.x = (e.clientX - rect.left + el.scrollLeft);
            this.y = (e.clientY - rect.top + el.scrollTop);
            
            this.rX = map(this.x, 0, el.clientWidth, -10, 10);
            this.rY = map(this.y, 0, el.clientHeight, -10, 10);

            el.style.transform = `rotateX(${this.rY}deg) rotateY(${this.rX}deg)`;
            el.style.perspective = "400px";
        };
        el.onmouseout = e => {
            el.style.transform = `rotateX(0) rotateY(0)`;
            el.style.perspective = '0'
        };
    }
}


function addToggleClass(burger,overlay,anotherBtn){
	burger.classList.toggle('active');
    overlay.classList.toggle('active');
    
    if(burger.classList.contains('active')){
        anotherBtn.classList.add('active');
    }else{
        anotherBtn.classList.remove('active');
    }
}

function removeClass(burger,overlay,anotherBtn){
	burger.classList.remove('active');
    overlay.classList.remove('active');
    anotherBtn.classList.remove('active');
}

function BurgerAndMenuContentOpenClose(){
	let contentOverlay = document.querySelector('.burger_overlay');	
	let getBurgerClick = document.querySelector('.burger_bar_container');		
    let headerBtn = document.querySelector('.header--nav-btn');
	getBurgerClick.addEventListener('click',function(){
		addToggleClass(contentOverlay, this,headerBtn)
	});

	contentOverlay.addEventListener('click',function(){
		removeClass(this,getBurgerClick,headerBtn);
	});
}


// Home Page APP Pages Slide
// ToDo create more smooth animations
const getTabs = document.querySelectorAll('.home--page-about-card-item');
const getTabsWrapper = document.querySelector('.home--page-about-project-cards');
let getActiveTab = document.querySelector('.home--page-about-card-item.active');
let activeDescription = document.querySelector(`.home--about-project-item[data-page-select="default"]`);
let defaultDescription = document.querySelector(`.home--about-project-item[data-page-select="default"]`);
let hoveredItem;
let hoveredTab;
let hoveredDescription;

function setActiveItem(item){
    let itemDataAttr = item.dataset.pageSelect;

    if(item == getActiveTab) return;

    if(getActiveTab && getActiveTab.classList.contains('active')){
        getActiveTab.classList.remove('active')
        document.querySelector(`.home--phone-image-inner[data-page-select="${getActiveTab.dataset.pageSelect}"]`).classList.remove('active');
        activeDescription.classList.remove('active');
        activeDescription.style.left = `0`
    }

    item.classList.add('active');
    getActiveTab = item;
    document.querySelector(`.home--phone-image-inner[data-page-select="${itemDataAttr}"]`).classList.add('active');
    document.querySelector(`.home--about-project-item[data-page-select="${itemDataAttr}"]`).classList.add('active');
    activeDescription = document.querySelector(`.home--about-project-item[data-page-select="${itemDataAttr}"]`)
    
    if(window.innerWidth < 1200 ){
        if(getActiveTab && getActiveTab.classList.contains('active')){
            activeDescription.style.left = `0`
        }

        if(defaultDescription.classList.contains('active')){
            defaultDescription.classList.remove('active');
            defaultDescription.style.left = `-200%`;
        }
        activeDescription.style.left = `-${activeDescription.offsetLeft}px`;
    }
}


if(window.innerWidth > 1200){
    getTabsWrapper.addEventListener('mouseout',(e) => {
        if(!e.toElement.classList.contains('home--page-about-project-cards')){
            defaultDescription.style.transform = `translate3d(0,0,0)`;
            activeDescription.style.transform = `translate3d(0,0,0)`;
        }
    })
}


getTabs.forEach(item => {
    
    if(window.innerWidth > 1200){
        item.addEventListener('click', (e) => {
            setActiveItem(e.currentTarget)
        });

        item.addEventListener('mouseover',(e) => {
            let itemDataAttr = item.dataset.pageSelect;
            let currentDescription = document.querySelector(`.home--about-project-item[data-page-select="${itemDataAttr}"]`);
            
            if(activeDescription.classList.contains('active')){
                activeDescription.classList.remove('active');
            }
            
            if(defaultDescription.classList.contains('active')){
                defaultDescription.classList.remove('active');
                defaultDescription.style.transform = `translate3d(-${(currentDescription.offsetLeft)}px,0,0)`;
            }
    
            document.querySelector(`.home--phone-image-inner[data-page-select="${itemDataAttr}"]`).classList.add('active');
            
            activeDescription.style.transform = `translate3d(-${(currentDescription.offsetLeft)}px,0,0)`;
    
            currentDescription.classList.add('active');
    
            activeDescription = currentDescription;
            
            activeDescription.style.transform = `translate3d(-${(activeDescription.offsetLeft)}px,0,0)`;
            
            
            hoveredTab = document.querySelector(`.home--phone-image-inner[data-page-select="${itemDataAttr}"]`);
            hoveredTab.style.zIndex = '20';
            hoveredDescription = document.querySelector(`.home--about-project-item[data-page-select="${itemDataAttr}"]`);
    
        });
    
        item.addEventListener('mouseout',(e) => {
            
            if(e.target.classList.contains('home--page-about-card-item')){
                defaultDescription.classList.add('active');
                activeDescription.classList.remove('active');
            }
            
            if(getActiveTab && (getActiveTab.dataset.pageSelect == hoveredTab.dataset.pageSelect)){
                hoveredTab.style.zIndex = '';
                return;
            }
    
    
            hoveredTab.style.zIndex = '';
            hoveredTab.classList.remove('active');
            hoveredDescription.classList.remove('active');
        });
    }

    if(window.innerWidth < 1199){
        item.addEventListener('touchstart', (e) => {
            console.log(e.currentTarget);
            
            setActiveItem(e.currentTarget)
        });

    }

});



// Popup Form Validation
const popupOpener = document.querySelectorAll('.formPopupOpener')
const popup = document.querySelector('.form--container');
const popupOverlay = document.querySelector('.partner-popup-overlay');
const closeIcon = document.querySelector('#closeIcon');

function openPopupForm() {
    popupOpener.forEach(el => {
        el.addEventListener('click', (e) => {
            e.preventDefault();
            document.body.style.overflow = "hidden";
            popup.classList.add('active');
            popupOverlay.classList.add('active');
        });

    })
    closeIcon.addEventListener('click', (e) => {
        document.body.style.overflow = "";
        popup.classList.remove('active');
        popupOverlay.classList.remove('active');
    });
}
function initContactForm() {
    jQuery('div.wpcf7 > form').wpcf7InitForm();
    jQuery('form.wpcf7-form')
        .each(function() {
            jQuery(this).find('img.ajax-loader').last().remove();
        });
}